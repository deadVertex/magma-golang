package main

import (
	"fmt"
	"github.com/go-gl/gl/v4.1-core/gl"
	"github.com/go-gl/glfw/v3.3/glfw"
	"github.com/go-gl/mathgl/mgl32"
	"log"
	"runtime"
	"strings"
)

type InputState struct {
	keyIsDown map[string]bool
	keyWasDown map[string]bool
}

func (inputState *InputState) BeginFrame() {
	for key,isDown := range inputState.keyIsDown {
		inputState.keyWasDown[key] = isDown
	}
}

func (inputState *InputState) SetKeyIsDown(key string, isDown bool) {
	inputState.keyIsDown[key] = isDown
}

type Camera struct {
	position mgl32.Vec3
}

const windowWidth = 800
const windowHeight = 600

const projectionLocation = 0
const viewLocation = 1
const modelLocation = 2

var inputState InputState = InputState{map[string]bool{}, map[string]bool{}}

var camera Camera = Camera{mgl32.Vec3{0, 0, 3}}

func init() {
	// GLFW event handling must run on the main OS thread
	runtime.LockOSThread()
}

func main() {
	log.Println("Hello World")

	if err := glfw.Init(); err != nil {
		log.Fatalln("failed to initialize glfw:", err)
	}
	defer glfw.Terminate()

	log.Println("glfw initialized")

	glfw.WindowHint(glfw.Resizable, glfw.False)
	glfw.WindowHint(glfw.ContextVersionMajor, 4)
	glfw.WindowHint(glfw.ContextVersionMinor, 3)
	glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)
	glfw.WindowHint(glfw.OpenGLForwardCompatible, glfw.True)
	window, err := glfw.CreateWindow(windowWidth, windowHeight, "Magma Go", nil, nil)
	if err != nil {
		log.Fatalln("failed to create window:", err)
	}
	window.MakeContextCurrent()

    window.SetKeyCallback(keyCallback)

	// Initialize Glow
	if err := gl.Init(); err != nil {
		panic(err)
	}

	version := gl.GoStr(gl.GetString(gl.VERSION))
	log.Println("OpenGL version:", version)

	program, err := newProgram(vertexShader, fragmentShader)
	if err != nil {
		panic(err)
	}

	vao, err := newVertexArrayObject()
	if err != nil {
		panic(err)
	}

	currentTime := glfw.GetTime()
	for !window.ShouldClose() {

		newTime := glfw.GetTime()
		frameTime := newTime - currentTime
		currentTime = newTime

		inputState.BeginFrame()

		glfw.PollEvents()

		update(float32(frameTime))
		render(program, vao)

		window.SwapBuffers()
	}
}

func keyCallback(window *glfw.Window, key glfw.Key, scancode int, action glfw.Action, mods glfw.ModifierKey) {
	if key == glfw.KeyEscape {
		window.SetShouldClose(true)
	}

	isDown := true
	if action == glfw.Release {
		isDown = false
	}

	keys := map[glfw.Key]string{
		glfw.KeyW: "W",
		glfw.KeyA: "A",
		glfw.KeyS: "S",
		glfw.KeyD: "D",
	}

	for mappedKey, keyName := range keys {
		if key == mappedKey {
			inputState.SetKeyIsDown(keyName, isDown)
		}
	}
}

func update(dt float32) {
	log.Println("FRAME TIME:", dt)

	speed := float32(4.0)
	var velocity mgl32.Vec3

	log.Println("W isDown:", inputState.keyIsDown["W"], "wasDown:", inputState.keyWasDown["W"])

	if inputState.keyIsDown["W"] {
		velocity = velocity.Add(mgl32.Vec3{0.0, 0.0, -1.0})
	}
	if inputState.keyIsDown["S"] {
		velocity = velocity.Add(mgl32.Vec3{0.0, 0.0, 1.0})
	}
	if inputState.keyIsDown["A"] {
		velocity = velocity.Add(mgl32.Vec3{-1.0, 0.0, 0.0})
	}
	if inputState.keyIsDown["D"] {
		velocity = velocity.Add(mgl32.Vec3{1.0, 0.0, 0.0})
	}

	log.Println(velocity)
	camera.position = camera.position.Add(velocity.Mul(speed * dt))
}

func render(program uint32, vao uint32) {
	gl.Enable(gl.DEPTH_TEST)
	gl.DepthFunc(gl.LESS)

	gl.ClearColor(0.6, 0.6, 0.9, 1.0)
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	gl.UseProgram(program)
	gl.BindVertexArray(vao)

	projection := mgl32.Perspective(mgl32.DegToRad(45.0), float32(windowWidth)/windowHeight, 0.1, 10.0)
	view := mgl32.Translate3D(-camera.position.X(), -camera.position.Y(), -camera.position.Z())
	model := mgl32.Ident4()

	gl.UniformMatrix4fv(projectionLocation, 1, false, &projection[0])
	gl.UniformMatrix4fv(viewLocation, 1, false, &view[0])
	gl.UniformMatrix4fv(modelLocation, 1, false, &model[0])

	gl.DrawArrays(gl.TRIANGLES, 0, 6*2*3)

}

var vertexShader = `
#version 330
#extension GL_ARB_explicit_uniform_location : require

layout(location = 0) uniform mat4 projection;
layout(location = 1) uniform mat4 view;
layout(location = 2) uniform mat4 model;

layout(location = 0) in vec3 inPosition;

void main() {
    gl_Position = projection * view * model * vec4(inPosition, 1);
}
` + "\x00"

var fragmentShader = `
#version 330
#extension GL_ARB_explicit_uniform_location : require

out vec4 outputColor;

void main() {
    outputColor = vec4(0.2, 0.2, 0.2, 1.0);
}
` + "\x00"

func newProgram(vertexShaderSource string, fragmentShaderSource string) (uint32, error) {
	vertexShader, err := compileShader(vertexShaderSource, gl.VERTEX_SHADER)
	if err != nil {
		return 0, err
	}

	fragmentShader, err := compileShader(fragmentShaderSource, gl.FRAGMENT_SHADER)
	if err != nil {
		return 0, err
	}

	program := gl.CreateProgram()

	gl.AttachShader(program, vertexShader)
	gl.AttachShader(program, fragmentShader)
	gl.LinkProgram(program)

	var status int32
	gl.GetProgramiv(program, gl.LINK_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetProgramiv(program, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetProgramInfoLog(program, logLength, nil, gl.Str(log))

		return 0, fmt.Errorf("failed to link program: %v", log)
	}

	gl.DeleteShader(vertexShader)
	gl.DeleteShader(fragmentShader)

	return program, nil
}

func compileShader(source string, shaderType uint32) (uint32, error) {
	shader := gl.CreateShader(shaderType)

	sources, free := gl.Strs(source)
	gl.ShaderSource(shader, 1, sources, nil)
	free()
	gl.CompileShader(shader)

	var status int32
	gl.GetShaderiv(shader, gl.COMPILE_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetShaderiv(shader, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetShaderInfoLog(shader, logLength, nil, gl.Str(log))

		// TODO: Should we use %w rather?
		return 0, fmt.Errorf("failed to compile %v: %v", source, log)
	}

	return shader, nil
}

func newVertexArrayObject() (uint32, error) {
	var vao uint32
	gl.GenVertexArrays(1, &vao)
	gl.BindVertexArray(vao)

	var vbo uint32
	gl.GenBuffers(1, &vbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
	gl.BufferData(gl.ARRAY_BUFFER, len(cubeVertices)*4, gl.Ptr(cubeVertices), gl.STATIC_DRAW)

	var stride int32 = 4 * 5
	gl.EnableVertexAttribArray(0)
	gl.VertexAttribPointer(0, 3, gl.FLOAT, false, stride, nil)

	return vao, nil
}

var cubeVertices = []float32{
	//  X, Y, Z, U, V
	// Bottom
	-1.0, -1.0, -1.0, 0.0, 0.0,
	1.0, -1.0, -1.0, 1.0, 0.0,
	-1.0, -1.0, 1.0, 0.0, 1.0,
	1.0, -1.0, -1.0, 1.0, 0.0,
	1.0, -1.0, 1.0, 1.0, 1.0,
	-1.0, -1.0, 1.0, 0.0, 1.0,

	// Top
	-1.0, 1.0, -1.0, 0.0, 0.0,
	-1.0, 1.0, 1.0, 0.0, 1.0,
	1.0, 1.0, -1.0, 1.0, 0.0,
	1.0, 1.0, -1.0, 1.0, 0.0,
	-1.0, 1.0, 1.0, 0.0, 1.0,
	1.0, 1.0, 1.0, 1.0, 1.0,

	// Front
	-1.0, -1.0, 1.0, 1.0, 0.0,
	1.0, -1.0, 1.0, 0.0, 0.0,
	-1.0, 1.0, 1.0, 1.0, 1.0,
	1.0, -1.0, 1.0, 0.0, 0.0,
	1.0, 1.0, 1.0, 0.0, 1.0,
	-1.0, 1.0, 1.0, 1.0, 1.0,

	// Back
	-1.0, -1.0, -1.0, 0.0, 0.0,
	-1.0, 1.0, -1.0, 0.0, 1.0,
	1.0, -1.0, -1.0, 1.0, 0.0,
	1.0, -1.0, -1.0, 1.0, 0.0,
	-1.0, 1.0, -1.0, 0.0, 1.0,
	1.0, 1.0, -1.0, 1.0, 1.0,

	// Left
	-1.0, -1.0, 1.0, 0.0, 1.0,
	-1.0, 1.0, -1.0, 1.0, 0.0,
	-1.0, -1.0, -1.0, 0.0, 0.0,
	-1.0, -1.0, 1.0, 0.0, 1.0,
	-1.0, 1.0, 1.0, 1.0, 1.0,
	-1.0, 1.0, -1.0, 1.0, 0.0,

	// Right
	1.0, -1.0, 1.0, 1.0, 1.0,
	1.0, -1.0, -1.0, 1.0, 0.0,
	1.0, 1.0, -1.0, 0.0, 0.0,
	1.0, -1.0, 1.0, 1.0, 1.0,
	1.0, 1.0, -1.0, 0.0, 0.0,
	1.0, 1.0, 1.0, 0.0, 1.0,
}
